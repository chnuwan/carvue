<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var [type]
     */
    protected $fillable = [
        'account_id',
        'id',
        'company',
        'title',
        'fName',
        'lName',
        'dName',
        'reference',
        'contactNo',
        'VATNo',
        'attention',
        'street',
        'town',
        'region',
        'postCode',
        'country',
        'billingAttention',
        'billingStreet',
        'billingTown',
        'billingRegion',
        'billingPostCode',
        'billingCountry',
        'billingMobile',
        'mobile',
        'phone',
        'email',
        'notes',
        'active'
    ];

}
