<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->integer('account_id')->unsigned();
            $table->integer('id')->unsigned()->autoIncrement();
            $table->tinyInteger('company')->unsigned();
            $table->char('title', 5);
            $table->string('fName', 50);
            $table->string('lName', 50);
            $table->string('dName', 50);
            $table->string('reference', 100);
            $table->string('contactNo', 100);
            $table->string('VATNo', 10);
            $table->string('attention', 50);
            $table->text('street')->nullable();
            $table->text('town')->nullable();
            $table->text('region')->nullable();
            $table->string('postCode', 10)->nullable();
            $table->text('country')->nullable();

            $table->string('billingAttention', 50)->nullable();
            $table->text('billingStreet')->nullable();
            $table->text('billingTown')->nullable();
            $table->text('billingRegion')->nullable();
            $table->string('billingPostCode', 10)->nullable();
            $table->text('billingCountry')->nullable();

            $table->string('mobile', 25);
            $table->string('phone', 25);
            $table->string('email', 25);

            $table->text('notes')->nullable();
            $table->tinyInteger('active')->default(1)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
