<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyProfiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->text('street');
            $table->text('town');
            $table->text('region');
            $table->string('postCode', 10);
            $table->text('country');
            $table->string('VATNo', 10);
            $table->string('logo', 250);
            $table->string('currency',3);
            $table->double('currencyRate',10,2);
            $table->string('mobile', 25);
            $table->string('phone', 25);
            $table->string('email', 25);
            $table->string('web',250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companyProfiles');
    }
}
