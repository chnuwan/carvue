<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customerVehicles', function (Blueprint $table) {
            $table->integer('customers_id')->unsigned();
            $table->integer('id')->unsigned()->autoIncrement();
            $table->text('registration');
            $table->string('make', 100);
            $table->string('model', 100);
            $table->string('derivative', 100)->nullable();
            $table->text('vin')->nullable();
            $table->date('regDate');
            $table->string('dateOfManufacture', 10);
            $table->text('mileageUnit')->nullable();
            $table->string('color', 50)->nullable();
            $table->string('paintCode', 50)->nullable();
            $table->text('trim')->nullable();
            $table->string('fuel', 10)->nullable();
            $table->string('doorPlan', 10)->nullable();

            $table->text('gearBox')->nullable();
            $table->text('gearBoxNo')->nullable();
            $table->text('CO2Emission')->nullable();
            $table->text('engineNo')->nullable();
            $table->text('engineCode')->nullable();
            $table->text('engineSize')->nullable();
            $table->text('power')->nullable();

            $table->text('keyNo')->nullable();
            $table->text('radioNo')->nullable();

            $table->date('MOTDueDate');
            $table->date('serviceDueDate');
            $table->date('warrantyEnds');

            $table->text('notes')->nullable();
            $table->tinyInteger('active')->default(1)->unsigned(); //renamed

            $table->foreign('customers_id')
                ->references('id')->on('customers');




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customerVehicles');
    }
}
